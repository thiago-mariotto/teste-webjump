

Teste WEBJUMP

---


## 🏁 Iniciando 

Instruções para execução do projeto

### PRÉ-Requisitos

php 7, compoesr, xampp, wamp ou algum outro servidor local, phpadmin.



### Installing

Clone este repositório para sua máquina

```
git clone git@bitbucket.org:thiago-mariotto/teste-webjump.git
```

Abra o diretorio no cmd ou git e exute o composer install para instalar as dependencias

```
composer install
```

Acesse phpadmin crie um novo banco de dados em sua máquina com um nome webjump, e execute a query dentro dele
```
CREATE TABLE produtos (sku varchar(25) UNIQUE, nome varchar(50), preco double, quantidade int, categoria varchar(255), descricao text ) ; CREATE TABLE categorias (codigo int, nome varchar(255))
```


Crie um arquivo .env e siga o exemplo .env.example, edite o .env criado com seus dados de acesso
EX:

```
DB_URL=localhost
DB_NAME=webjump
DB_USER=root
DB_PASSWORD=
```

Acesse a página index pelo seu servidor host ;]

## ⛏️ Construido

- [PHP 7](https://www.php.net/manual/pt_BR/migration70.new-features.php) - Backend


## 🎉 Dúvidas

- thiagomariotto@outlook.com.br
