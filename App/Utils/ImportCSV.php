<?php

/**
 * @param string $path
 * 
 * @return array
 */
function readCSV($path)
{
    $row = 1;
    $products = [];

    if (($handle = fopen($path, "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 1001, ";")) !== FALSE) {
            //first line normally used for the spreadsheet title
            if ($row == 1) {
                $row++;
                continue;
            }

            $num = count($data);

            $row++;
            for ($c = 0; $c < $num; $c++) {
                $products[$row][$c] = $data;
            }
        }

        fclose($handle);
    } else {
        return exit;
    }

    return $products;
}

//to use this function you need import this file and add readCSV function