<?php

/**
 *
 * verificaCampoVazio: Verifica se todos os campos foram preenchidos
 *
 * @param array $campos
 * @return boolean
 * 
 */
function verifyEmptyFields($campos)
{
    foreach ($campos as $fields) :
        if (empty($fields)) :
            echo "Erro na camada Model, há um campo vazio.";
            var_dump($fields);
            exit;
        endif;
    endforeach;
}
