<?php 

/**
 * 
 * VerificaInt: Verifica se variável esta na tipagem correta
 * 
 * @param $var
 * @return boolean
 * 
 */
function verifyInt($var){
    if(!is_int($var)){
        echo "Erro na validação, o campo deve ser do tipo Inteiro.";
        exit;
    }
}

function verifyString($var){
    if(!is_string($var)){
        echo "Erro na validação, o campo deve ser do tipo Texto.";
        exit;
    }
}


function verifyFloat($var){
    if(!is_float($var)){
        echo "Erro na validação, o campo deve ser do tipo Float.";
        exit;
    }
}

