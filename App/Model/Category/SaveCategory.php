<?php

include_once(__DIR__.'../../../../global.php');

/**
 *
 * Interface de Crud
 *  
 */
interface iCategoriaCrud
{
    public function create($object);
    public function read($param);
    public function update($object);
    public function delete($param);
}

/**
 * 
 * Class GravarCateria: CRUD de Categorias
 */
class SaveCategory implements iCategoriaCrud
{
    private $instanciaConexaoPdoAtiva;
    private $tabela;

    public function __construct()
    {
        $this->instanciaConexaoPdoAtiva = PdoConnection::getInstancia();
        $this->tabela = "categorias";
    }

    /**
     *
     * create: Insere informações no banco de dados
     *
     * @param object $objeto
     * @return boolean
     */
    public function create($objeto)
    {   
        $codigo = $objeto->getCodigo();
        $nome = $objeto->getNome();

        //save variables in array to test
        $campos = [$codigo, $nome];

        //verify if in array $campos != empty
        verifyEmptyFields($campos);

        //verify if codigo is int
        verifyInt($codigo);

        //executa query
        $sqlStmt =
            "INSERT INTO {$this->tabela} (codigo, nome) 
            VALUES (:codigo, :nome)";

        try {
            $operacao = $this->instanciaConexaoPdoAtiva->prepare($sqlStmt);
            $operacao->bindValue(":codigo", $codigo, PDO::PARAM_STR);
            $operacao->bindValue(":nome", $nome, PDO::PARAM_STR);
            

            if ($operacao->execute()) {
                if ($operacao->rowCount() > 0) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (PDOException $excecao) {
            echo $excecao->getMessage();
        }
    }
    /**
     *
     * read: Retorna um objeto refletindo um produto
     *
     * @param int $codigo
     * @return object
     */


    public function read($codigo)
    {
        $sqlStmt = "SELECT * from {$this->tabela} WHERE codigo=:codigo";
        try {
            $operacao = $this->instanciaConexaoPdoAtiva->prepare($sqlStmt);
            $operacao->bindValue(":codigo", $codigo, PDO::PARAM_INT);
            $operacao->execute();
            $getRow = $operacao->fetch(PDO::FETCH_OBJ);
            $codigo = $getRow->codigo;
            $nome = $getRow->nome;
            $objeto = new Category($codigo, $nome);
            $objeto->setCodigo($codigo);
            return $objeto;
        } catch (PDOException $excecao) {
            echo $excecao->getMessage();
        }
    }

    /**
     *
     * update: atualiza um produto
     *
     * @param object $objeto
     * @return boolean
     */
    public function update($objeto)
    {
        $codigo = $objeto->getCodigo();
        $nome = $objeto->getNome();

        $sqlStmt = "UPDATE {$this->tabela} SET codigo=:codigo, nome=:nome WHERE codigo=:codigo";
        try {
            $operacao = $this->instanciaConexaoPdoAtiva->prepare($sqlStmt);
            $operacao->bindValue(":codigo", $codigo, PDO::PARAM_INT);
            $operacao->bindValue(":nome", $nome, PDO::PARAM_STR);
            if ($operacao->execute()) {
                if ($operacao->rowCount() > 0) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (PDOException $excecao) {
            echo $excecao->getMessage();
        }
    }
    /**
     *
     * DELETE exclui um produto no banco de dados conforme informado por id
     *
     * @param int $id
     * @return boolean
     */
    public function delete($codigo)
    {
        $sqlStmt = "DELETE FROM {$this->tabela} WHERE codigo=:codigo";
        try {
            $operacao = $this->instanciaConexaoPdoAtiva->prepare($sqlStmt);
            $operacao->bindValue(":codigo", $codigo, PDO::PARAM_INT);
            if ($operacao->execute()) {
                if ($operacao->rowCount() > 0) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (PDOException $excecao) {
            echo $excecao->getMessage();
        }
    }
}
