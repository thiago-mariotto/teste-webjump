<?php

require_once(__DIR__ . '../../../../global.php');

/**
 * Executa conexao PDO 
 */
class PdoConnection{
    private static $instancia;

     // block instace
     private function __construct() { }
     // block clone
     private function __clone() { }
    
     //block Unserialize
     private function __wakeup() { }

     /**
      * @return object PDO connection
      */
     public static function getInstancia() {
         if(!isset(self::$instancia)) {
              try {
                  $dsn = 'mysql:host=localhost;dbname=webjump';
                  $usuario = $_ENV['DB_USER'];
                  $senha = $_ENV['DB_PASSWORD'];
                  
                  //Instance of a new PDO object 
                  self::$instancia = new PDO( $dsn, $usuario, $senha );
                  
                  //xception of type PDOException with error code
                  self::$instancia->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
              
              } catch ( PDOException $excecao ){
                  echo $excecao->getMessage();
                  // end app
                  exit();
              }
          }
          return self::$instancia;
         }
}


