<?php

include_once(__DIR__.'../../../../global.php');

interface iProductCrud
{
   public function create($object);
   public function read($param);
   public function update($object);
   public function delete($param);
}

/**
 * 
 * Descricao: Utilizada para armazenar o produto no banco de dados 
 * 
 */
class SaveProduct implements iProductCrud
{

   private $instacePdoActive;
   private $table;

   public function __construct()
   {
      $this->instacePdoActive = PdoConnection::getInstancia();
      $this->table = "produtos";
   }
   /**
    *
    * create: Insert product in database
    *
    * @param object $object
    * @return boolean
    */
   public function create($object)
   {
      $nome = $object->getNome();
      $sku = $object->getSku();
      $descricao = $object->getDescricao();
      $quantidade = $object->getQuantidade();
      $preco = $object->getPreco();
      $categoria = $object->getCategoria();

      //add fields to an array to check if they were sent
      $campos = [$sku, $nome, $descricao, $quantidade, $preco, $categoria];
      verifyEmptyFields($campos);

      //changes ',' by '.'
      $preco = str_replace([','], '.', $preco);
     
      // convert price string to float
      $preco = (float) $preco;
      $quantidade = intval($quantidade);

      //verifica tipagem de dados
      verifyFloat($preco);
      verifyInt($quantidade);

      //executa query
      $sqlStmt =
         "INSERT INTO {$this->table} (nome, sku, descricao, quantidade, preco, categoria) 
            VALUES (:nome, :sku, :descricao, :quantidade, :preco, :categoria )";

      try {
         $operation = $this->instacePdoActive->prepare($sqlStmt);
         $operation->bindValue(":nome", $nome, PDO::PARAM_STR);
         $operation->bindValue(":sku", $sku, PDO::PARAM_STR);
         $operation->bindValue(":descricao", $descricao, PDO::PARAM_STR);
         $operation->bindValue(":quantidade", $quantidade, PDO::PARAM_STR);
         $operation->bindValue(":preco", $preco, PDO::PARAM_STR);
         $operation->bindValue(":categoria", $categoria, PDO::PARAM_STR);

         if ($operation->execute()) {
            if ($operation->rowCount() > 0) {
               return true;
            } else {
               return false;
            }
         } else {
            return false;
         }
      } catch (PDOException $excecao) {
         echo $excecao->getMessage();
      }
   }


   /**
    *
    * read: Retorna um object refletindo um contato
    *
    * @param int $id
    * @return object
    */
   public function read($id)
   {
      $sqlStmt = "SELECT * from {$this->table} WHERE id=:id";
      try {
         $operation = $this->instacePdoActive->prepare($sqlStmt);
         $operation->bindValue(":id", $id, PDO::PARAM_INT);
         $operation->execute();
         $getRow = $operation->fetch(PDO::FETCH_OBJ);
         $nome = $getRow->nome;
         $sku = $getRow->sku;
         $descricao = $getRow->descricao;
         $quantidade = $getRow->quantidade;
         $preco = $getRow->preco;
         $categoria = $getRow->categoria;
         $object = new Produto($nome, $sku, $descricao, $quantidade, $preco, $categoria);
         $object->setId($id);
         return $object;
      } catch (PDOException $excecao) {
         echo $excecao->getMessage();
      }
   }

   /**
    *
    * update: atualiza um produto
    *
    * @param object $object
    * @return boolean
    */
   public function update($object)
   {
      $id = $object->getId();
      $sku = $object->getSku();
      $descricao = $object->getDescricao();
      $quantidade = $object->getQuantidade();
      $preco = $object->getPreco();
      $categoria = $object->getCategoria();

      $sqlStmt = "UPDATE {$this->table} SET sku=:sku, descricao=:descricao, quantidade=:quantidade, preco=:preco, categoria=:categoria WHERE id=:id";
      try {
         $operation = $this->instacePdoActive->prepare($sqlStmt);
         $operation->bindValue(":id", $id, PDO::PARAM_INT);
         $operation->bindValue(":sku", $sku, PDO::PARAM_STR);
         $operation->bindValue(":descricao", $descricao, PDO::PARAM_STR);
         $operation->bindValue(":quantidade", $quantidade, PDO::PARAM_STR);
         $operation->bindValue(":preco", $preco, PDO::PARAM_STR);
         $operation->bindValue(":categoria", $categoria, PDO::PARAM_STR);
         if ($operation->execute()) {
            if ($operation->rowCount() > 0) {
               return true;
            } else {
               return false;
            }
         } else {
            return false;
         }
      } catch (PDOException $excecao) {
         echo $excecao->getMessage();
      }
   }
   /**
    *
    * DELETE deletes a product in the database as reported by id
    *
    * @param int $id
    * @return boolean
    */
   public function delete($id)
   {
      $sqlStmt = "DELETE FROM {$this->table} WHERE id=:id";
      try {
         $operation = $this->instacePdoActive->prepare($sqlStmt);
         $operation->bindValue(":id", $id, PDO::PARAM_INT);
         if ($operation->execute()) {
            if ($operation->rowCount() > 0) {
               return true;
            } else {
               return false;
            }
         } else {
            return false;
         }
      } catch (PDOException $excecao) {
         echo $excecao->getMessage();
      }
   }
}
