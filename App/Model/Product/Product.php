<?php

/**
* [Cria um novo produto]
*/
    class Product{ 

        private $nome;
        private $sku;
        private $descricao;
        private $quantidade;
        private $preco;
        private $categoria;
        
        /**
         * @param mixed $nome
         * @param mixed $sku
         * @param mixed $descricao
         * @param mixed $quantidade
         * @param mixed $preco
         * @param mixed $categoria
         */
        public function __construct($nome, $sku, $descricao, $quantidade, $preco, $categoria ) {
            $this->nome = $nome;
            $this->sku = $sku;
            $this->descricao = $descricao;
            $this->quantidade = $quantidade; 
            $this->preco = $preco;
            $this->categoria = $categoria;
        }


        /**
         * Get the value of nome
         */ 
        public function getNome()
        {
                return $this->nome;
        }

        /**
         * Set the value of nome
         *
         * @return  self
         */ 
        public function setNome($nome)
        {
                $this->nome = $nome;

                return $this;
        }

        /**
         * Get the value of sku
         */ 
        public function getSku()
        {
                return $this->sku;
        }

        /**
         * Set the value of sku
         *
         * @return  self
         */ 
        public function setSku($sku)
        {
                $this->sku = $sku;

                return $this;
        }

        /**
         * Get the value of descricao
         */ 
        public function getDescricao()
        {
                return $this->descricao;
        }

        /**
         * Set the value of descricao
         *
         * @return  self
         */ 
        public function setDescricao($descricao)
        {
                $this->descricao = $descricao;

                return $this;
        }

        /**
         * Get the value of quantidade
         */ 
        public function getQuantidade()
        {
                return $this->quantidade;
        }

        /**
         * Set the value of quantidade
         *
         * @return  self
         */ 
        public function setQuantidade($quantidade)
        {
                $this->quantidade = $quantidade;

                return $this;
        }

        /**
         * Get the value of preco
         */ 
        public function getPreco()
        {
                return $this->preco;
        }

        /**
         * Set the value of preco
         *
         * @return  self
         */ 
        public function setPreco($preco)
        {
                $this->preco = $preco;

                return $this;
        }

        /**
         * Get the value of categoria
         */ 
        public function getCategoria()
        {
                return $this->categoria;
        }

        /**
         * Set the value of categoria
         *
         * @return  self
         */ 
        public function setCategoria($categoria)
        {
                $this->categoria = $categoria;

                return $this;
        }

        /**
         * Get the value of id
         */ 
        public function getId()
        {
                return $this->id;
        }

        /**
         * Set the value of id
         *
         * @return  self
         */ 
        public function setId($id)
        {
                $this->id = $id;

                return $this;
        }
    }  


