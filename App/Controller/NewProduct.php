<?php

include_once(__DIR__.'../../../global.php');

function my_autoload ($pClassName) {
    include(__DIR__ . "../../Model/Product/{$pClassName}.php");
}
spl_autoload_register("my_autoload");

//organizes categories
$category = null;
foreach ($_REQUEST['category'] as $value){
        $category = $value . "|" . $category;
}


//receive data from frontend
$sku = $_POST["sku"];
$name = $_POST["name"];
$price = $_POST["price"];
$quantity = $_POST["quantity"];
$description = $_POST["description"];

var_dump($category);
// instance new Product
$product = new Product($name, $sku, $description, $quantity, $price, $category);

// instace new data save
$saveProduct = new SaveProduct();

//tell create method and send $product param
if ($saveProduct->create($product)) {
    $_SESSION['msg'] = 'Produto cadastrado com sucesso.';
    header('Location: ../View/dashboard.html');
} else {
    $_SESSION['msg'] = 'Falha ao criar produto.';
    header('Location: ../View/addProduct.html');
}
