<?php

include_once(__DIR__.'../../../global.php');

function my_autoload ($pClassName) {
    include(__DIR__ . "../../Model/Category/{$pClassName}.php");
}
spl_autoload_register("my_autoload");

//receive data from frontend
$codigo = $_POST["category-code"];
$nome = $_POST["category-name"];

//change codigo to int
$codigo = intval($codigo);

//instace new category
$categoria = new Category($codigo, $nome);

$PersitenciaCategoria = new SaveCategory();

if ($PersitenciaCategoria->create($categoria)) {
    $_SESSION['msg'] = 'Categoria criada com sucesso';
    header('Location: ../View/dashboard.html');
    
} else {
    $_SESSION['msg'] = 'Erro ao criar categoria';
    header('Location: ../View/addCategory.html');
}
