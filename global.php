<?php 

include_once(__DIR__ . '/vendor/autoload.php');
include_once(__DIR__ . '../App/Model/Connection/PdoConnection.php');
include_once(__DIR__ . '../App/Utils/ImportCSV.php');
include_once(__DIR__ . '../App/Utils/VerifyFields.php');
include_once(__DIR__ . '../App/Utils/VerifyType.php');

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

spl_autoload_extensions(".php"); // comma-separated list
spl_autoload_register();